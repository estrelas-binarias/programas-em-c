#include <stdio.h>

int main() {
	int *puteiro;
	int numero = 10;
	puteiro = &numero;
	printf("um: %i %i %p %p\n", *puteiro, numero, &numero, puteiro);
	*puteiro = 20;
	printf("dois: %i %i %p %p\n", *puteiro, numero, &numero, puteiro);
	return 0;
};
