// Muito foda.

#include <stdio.h>

void troca(int *ptr_01, int *ptr_02) {
	int auxiliar;
	auxiliar = *ptr_01;
	*ptr_01 = *ptr_02;
	*ptr_02 = auxiliar;
};

// os ponteiros prt_01 e ptr_02 recebem os ponteiros ponteiro_01 e ponteiro_02 que, por sua
// vez, possuem os endereços de memória de numero_01 e numero_02, os quais possuem os valores
// inseridos.

int main() {
	int numero_01, numero_02;
	int *ponteiro_01, *ponteiro_02;
	scanf("%i %i", &numero_01, &numero_02);
	ponteiro_01 = &numero_01;
	ponteiro_02 = &numero_02;
	troca(ponteiro_01, ponteiro_02);
	printf("%i %i", numero_01, numero_02);
	return 0;
};
