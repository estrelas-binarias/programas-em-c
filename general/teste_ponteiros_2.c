#include <stdio.h>

int main() {
	char a;
	char *ponteiro;
	ponteiro = &a;
	*ponteiro = 'b';
	printf("primeiro: %c %p %c", *ponteiro, ponteiro, a);
	ponteiro++;
	printf("segundo: %c %p %c", *ponteiro, ponteiro, a);
	return 0;
};
